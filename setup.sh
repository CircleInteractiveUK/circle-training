#!/bin/bash

echo "Which training environment would you like to set up?"
echo "You can choose one at time."
echo "1) DNS"
echo "2) Bash"
echo
read CHOICE
if [[ $CHOICE == 1 ]]; then
    echo "Setup DNS training"
elif [[ $CHOICE == 2 ]]; then
    echo "Setup Bash training"
    cd bash
    ./setup.sh
fi
exec bash
